package br.alura.refatoracao.cap6;

public enum Moedas {

	DOLAR(2.7),
	EURO(3.0);
	
	private double taxa;

	Moedas(double taxa) {
		this.taxa = taxa;
		
	}
	
	public double getTaxa() {
		return taxa;
	}
}
