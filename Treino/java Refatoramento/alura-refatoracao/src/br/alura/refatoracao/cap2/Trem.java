package br.alura.refatoracao.cap2;

import java.util.List;

public class Trem {

	private List<Vagao> vagoes;
	private int capacidade;
	
	public boolean podeReservar(int lugaresAReservar) {
		
		int vagoesLivres = capacidade -  vagoesJaReservados();
		return vagoesLivres > lugaresAReservar; 
	}

	private int vagoesJaReservados() {
		int vagaoReservado = 0;
		for(Vagao vagao : vagoes) {
			vagaoReservado += vagao.reservados();
		}
		return vagaoReservado;
	}
	
}