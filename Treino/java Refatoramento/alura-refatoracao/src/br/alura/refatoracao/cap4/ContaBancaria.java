package br.alura.refatoracao.cap4;

public abstract class ContaBancaria {
	protected String titular;
	protected double saldo;
	
	public ContaBancaria(String titular, double saldo) {
		this.titular = titular;
		this.saldo = saldo;
	}
	
	public void sacar(double valor){
		this.saldo -= valor;
	}
	
	public void depositar(double valor){
		this.saldo += valor;
	}
	
	public double getSaldo() {
		return saldo;
	}

	public String getTitular() {
		return titular;
	}
}
