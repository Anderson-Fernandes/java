package br.alura.refatoracao.cap4;

public class ContaPJ extends ContaBancaria{

	double taxaDeTransferencia = 0.2;

	public ContaPJ(String titular, double saldoInicial) {
		super(titular, saldoInicial);
	}
	
	public void saca(double valor) {
		super.sacar(valor - taxaDeTransferencia);
	}
	
	public void deposita(double valor) {
		saldo += valor - taxaDeTransferencia;
	}
	
}
