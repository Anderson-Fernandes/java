package br.alura.refatoracao.cap4;

public class ContaPF extends ContaBancaria{

	double taxaDeTransferencia = 0.1;
	
	public ContaPF(String titular, double saldoInicial) {
		super(titular, saldoInicial);
	}
	
	public void saca(double valor) {
		super.sacar(valor - taxaDeTransferencia);
	}
	
	public void deposita(double valor) {
		super.depositar(valor - taxaDeTransferencia);
	}
	
	
}
