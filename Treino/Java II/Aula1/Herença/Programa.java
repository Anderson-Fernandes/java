class Programa{
	public static void main(String [] args){
		Funcionario joao = new Funcionario();
		joao.setSalario(1000.0);

		System.out.println(joao.getBonus());

		Gerente joakim = new Gerente();
		joakim.setSalario(2000.0);

		System.out.println(joakim.getBonus());

		TotalizadorBonus totalizador = new TotalizadorBonus();

		totalizador.adiciona(joao);
		totalizador.adiciona(joakim);

		System.out.println(totalizador.getTotal());
	}
}