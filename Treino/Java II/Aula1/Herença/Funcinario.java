class Funcionario{
	private String nome;
	protected double salario;

	public void setSalario(double salario){
		this.salario = salario;
	}

	public double getBonus(){

		return this.salario * 0.2;
	}
}

class TotalizadorBonus{
	private double total = 0;

	public void adiciona(Funcionario f){
		total += f.getBonus();
	}

	

	public double getTotal(){
		return this.total;
	}
}

class Gerente extends Funcionario{

	public double getBonus(){

		return this.salario * 0.3;
	}
}

class Desenvolvedor extends Funcionario{
	public double getBonus(){
		return this.salario * 0.25;
	}

}