class Conta{
	protected double saldo;

 
	public double getSaldo(){
		return this.saldo;
	}

	public void deposita(double valor){
		this.saldo += valor;
	}

	public void sacar(double valor){
		if(valor > this.saldo){
			System.out.println("Saldo insuficiente");
		}else{
			this.saldo -= valor;
		}
	}

	public void atualiza(double taxa){
		this.saldo += getSaldo() * taxa;
	}
}

class ContaCorrente extends Conta{

	public String nome = "american";
	public void atualiza(double taxa){
		this.saldo += getSaldo() * taxa * 2;
	}
}

class ContaPoupanca extends Conta{
	public void atualiza(double taxa){
		this.saldo += getSaldo() * taxa * 3;

	}

	public void deposita(double valor){
		this.saldo += valor - 0.10;
	}
}

class AtualizadorConta{
	private double saldoTotal =0;
	private double selic;

	public AtualizadorConta(double selic){
		this.selic = selic;
	}

	public void roda(Conta c){
		System.out.println(c.getSaldo());
		c.atualiza(this.selic);
		this.saldoTotal += c.getSaldo();
		System.out.println(c.getSaldo());
	}

	public double getSaldoTotal(){
		return this.saldoTotal;
	}
}