class Programa{
	public static void main(String[] args){
		Conta c = new Conta();
		Conta cc = new ContaCorrente();
		Conta cp = new ContaPoupanca();

		AtualizadorConta ac = new AtualizadorConta(0.01);

		c.deposita(1000);
		cc.deposita(1000);
		cp.deposita(1000);

		ac.roda(c);
		ac.roda(cc);
		ac.roda(cp);

		System.out.println("Saldo Total =" + ac.getSaldoTotal());
	}
}