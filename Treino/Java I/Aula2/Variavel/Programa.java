public class Programa{
	public static void main(String[] args){
		int idadeJoao = 18, idadeMaria = 21;

		int somaIdades = idadeJoao + idadeMaria;

		System.out.println(somaIdades);

		final double pi = 3.1315;
		System.out.println(pi);

		boolean amigo = true;
		boolean inimigo = !amigo;

		System.out.println(amigo);
		System.out.println(inimigo);

		boolean maiorDeIdade = idadeJoao>=18;
		System.out.println(maiorDeIdade);
		char letra = 'M';
		System.out.println(letra);

		String nomeCompleto = "João da Silva";
		System.out.println(nomeCompleto);
	}
}