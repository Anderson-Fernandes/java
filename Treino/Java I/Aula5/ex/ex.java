class Funcionario{
	private String nome;
	private String departamento;
	private double salario;
	private Data dataEntrada = new Data();
	private String rg;

	public double getSalario(){
		return this.salario;
	}

	public void setSalario(double salario){
		this.salario = salario;
	}

	public void receberAumento(double aumento){
		this.salario += aumento;
	}

	public double getGanhoAnual(){
		return this.salario * 12;
	}

	public void mostrar(){
		System.out.println("Nome: " + this.nome);
		System.out.println("Departamento: " + this.departamento);
		System.out.println("Salário: " + getSalario());
		System.out.println("Data de Entrada: " +this.dataEntrada.escreverData());
		System.out.println("RG: " + this.rg);
	}
}

class Data{
	int dia;
	int mes;
	int ano;

	void preencheData(int dia, int mes, int ano){
		this.dia = dia;
		this.mes = mes;
		this.ano = ano;
	}

	String escreverData(){
		return dia+"/"+mes+"/"+ano;
	}
}

class Empresa{
	private String cnpj;
	private int livre = 0;
	private Funcionario[] empregados;


	public Empresa(String cnpj){
		this.cnpj = cnpj;
	}
	public void adicionar(Funcionario f){
		this.empregados[livre] = f; 
		livre++;

	}

	public void setFuncionario(int qtdFuncionario){
		this.empregados = new Funcionario[qtdFuncionario];
	}

	public Funcionario getFuncionario(int posicao){
		return this.empregados[posicao];
	}

	public void mostrarEmpregados(){
		for(int i = 0; i < this.livre; i++){
			System.out.println("Funcionario da Posição '"+ i + "'\nSalario: " + this.empregados[i].getSalario());
		}
	}

	public void mostrarTodasInformacoes(){
		for(int i = 0; i< this.livre; i++){
			this.empregados[i].mostrar();
		}
	}

	public boolean contem(Funcionario f){
		for(int i = 0 ; i<this.livre; i++){
			if(f == this.empregados[i]){
				return true;
			}
		}
		return false;
	}
}

class Programa{
	public static void main(String[] args){
	
		Empresa empresa = new Empresa();
		empresa.setFuncionario(5);

		for(int i=0; i<5;i++){
			Funcionario f = new Funcionario ();
			f.setSalario(1000 + i * 100);
			empresa.adicionar(f);
		}

		System.out.println(empresa.getFuncionario(2).getSalario());

	}
}