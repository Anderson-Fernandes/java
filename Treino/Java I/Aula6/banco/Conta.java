class Conta{
 	int numero;
	Pessoa titular;
	private double saldo;
	private double limite;

	public Conta(int numero, double limite){
		this(numero,limite,0);
	}

	public Conta(int numero, double limite, double saldoInicial){
		this.numero = numero;
		this.limite = limite;
		this.saldo = saldoInicial;
	}


	public void setLimite(double limite){
		this.limite = limite;
	}

	public double getSaldo(){
		return this.saldo;
	}

	public void deposita(double valor){
		this.saldo += valor ;
	}

	public void saca(double valor){
		if(valor > this.saldo + this.limite){
			System.out.println("Saldo Insuficiente!");
		}else{
			this.saldo -=valor;
			
		}
	}

	public void transfere (double valor, Conta destino){
		if(this.saldo >= valor){
			this.saca(valor);
			destino.saldo += valor;
		}
	}
}

class Pessoa{
	String nome;
	private String cpf;
	String dataNasc;
	String endereco;
}

