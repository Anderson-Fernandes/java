
class Empresa{
	private String cnpj;
	private int livre = 0;
	private Funcionario[] empregados;


	public Empresa(String cnpj, int quantidadeFuncionarios){
		setFuncionario(quantidadeFuncionarios);
		this.cnpj = cnpj;
	}
	
	public void adicionar(Funcionario f){
		this.empregados[livre] = f; 
		livre++;

	}

	private void setFuncionario(int qtdFuncionario){
		this.empregados = new Funcionario[qtdFuncionario];
	}

	public Funcionario getFuncionario(int posicao){
		return this.empregados[posicao];
	}

	public void mostrarEmpregados(){
		for(int i = 0; i < this.livre; i++){
			System.out.println("Funcionario da Posição '"+ i + "'\nSalario: " + this.empregados[i].getSalario());
		}
	}

	public void mostrarTodasInformacoes(){
		for(int i = 0; i< this.livre; i++){
			this.empregados[i].mostrar();
		}
	}

	public boolean contem(Funcionario f){
		for(int i = 0 ; i<this.livre; i++){
			if(f == this.empregados[i]){
				return true;
			}
		}
		return false;
	}
}

