class Funcionario{
	private String nome;
	private String departamento;
	private double salario;
	public Data dataEntrada = new Data();
	private String rg;

	public Funcionario(String nome){
		this.nome = nome;
	}

	public Funcionario(){

	}

	public double getSalario(){
		return this.salario;
	}

	public void setSalario(double salario){
		this.salario = salario;
	}

	public void receberAumento(double aumento){
		this.salario += aumento;
	}

	public double getGanhoAnual(){
		return this.salario * 12;
	}

	public void mostrar(){
		System.out.println("Nome: " + this.nome);
		System.out.println("Departamento: " + this.departamento);
		System.out.println("Salário: " + getSalario());
		System.out.println("Data de Entrada: " +this.dataEntrada.escreverData());
		System.out.println("RG: " + this.rg);
	}
}
