class Conta{
 	int numero;
	Pessoa titular;
	double saldo;
	int agencia;

	void deposita(double valor){
		this.saldo += valor ;
	}

	void saca(double valor){
		if(this.saldo >= valor){
			this.saldo -=valor;
		}
	}

	void transfere (double valor, Conta destino){
		if(this.saldo >= valor){
			this.saldo -= valor;
			destino.saldo += valor;
		}
	}
}

class Pessoa{
	String nome;
	String cpf;
	String dataNasc;
}

class Programa{
	public static void main(String[] args){
		Conta anderson = new Conta();
		anderson.numero = 123;
		anderson.saldo = 800.0;
		//anderson.titular = "Anderson";
		anderson.agencia = 842;
		anderson.titular = new Pessoa();
		anderson.titular.nome = "Anderson";
		anderson.titular.cpf = "123456798";
		anderson.titular.dataNasc = "22/07/1997";


		Conta guilherme = new Conta();
		guilherme.numero = 256;
		guilherme.saldo = 1200.0;


		System.out.println(anderson.saldo);
		System.out.println(guilherme.saldo);

		anderson.transfere(9000,guilherme);

		System.out.println(anderson.saldo);
		System.out.println(guilherme.saldo);

	}
}