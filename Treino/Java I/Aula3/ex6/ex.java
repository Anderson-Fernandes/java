class Funcionario{
	String nome;
	String departamento;
	double salario;
	Data dataEntrada;
	String rg;

	void receberAumento(double aumento){
		this.salario += aumento;
	}

	double calcularGanhoAnual(){
		return this.salario * 12;
	}

	void mostrar(){
		System.out.println("Nome: " + this.nome);
		System.out.println("Departamento: " + this.departamento);
		System.out.println("Salário: " + this.salario);
		System.out.println("Data de Entrada: " +this.dataEntrada.escreverData());
		System.out.println("RG: " + this.rg);
	}
}

class Data{
	int dia;
	int mes;
	int ano;

	void preencheData(int dia, int mes, int ano){
		this.dia = dia;
		this.mes = mes;
		this.ano = ano;
	}

	String escreverData(){
		return dia+"/"+mes+"/"+ano;
	}
}

class Programa{
	public static void main(String[] args){
		Funcionario f1 = new Funcionario();
		f1.nome = "Anderson";
		f1.salario = 100;
		f1.dataEntrada = new Data();
		//f1.dataEntrada.preencheData(27,03,2016);
		f1.mostrar();
		Funcionario f2=f1;

		if(f1 == f2){ //Cairá no Else, pois está comparando a referencia.
			System.out.println("Iguais");
		}else{
			System.out.println("Diferentes");
		}
	}
}