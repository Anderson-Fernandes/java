class Funcionario{
	String nome;
	String departamento;
	double salario;
	String dataEntrada;
	String rg;

	void receberAumento(double aumento){
		this.salario += aumento;
	}

	double calcularGanhoAnual(){
		return this.salario * 12;
	}

	void mostrar(){
		System.out.println("Nome: " + this.nome);
		System.out.println("Departamento: " + this.departamento);
		System.out.println("Salário: " + this.salario);
		System.out.println("Data de Entrada: " +this.dataEntrada);
		System.out.println("RG: " + this.rg);
	}
}

class Programa{
	public static void main(String[] args){
		Funcionario f1 = new Funcionario();
		f1.nome = "Anderson";
		f1.salario = 100;

		Funcionario f2 = new Funcionario();
		f2.nome = "Anderson";
		f2.salario = 100;

		if(f1 == f2){ //Cairá no Else, pois está comparando a referencia.
			System.out.println("Iguais");
		}else{
			System.out.println("Diferentes");
		}
	}
}