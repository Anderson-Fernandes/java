class Funcionario{
	String nome;
	String departamento;
	double salario;
	String dataEntrada;
	String rg;

	void receberAumento(double aumento){
		this.salario += aumento;
	}

	double calcularGanhoAnual(){
		return this.salario * 12;
	}

	void mostrar(){
		System.out.println("Nome: " + this.nome);
		System.out.println("Departamento: " + this.departamento);
		System.out.println("Salário: " + this.salario);
		System.out.println("Data de Entrada: " +this.dataEntrada);
		System.out.println("RG: " + this.rg);
	}
}

class Programa{
	public static void main(String[] args){
		Funcionario anderson = new Funcionario();
		anderson.nome = "Anderson";
		anderson.departamento = "Working";
		anderson.salario = 500.0;
		anderson.dataEntrada = "27/03/2016";
		anderson.rg = "123.123.12/4";

		System.out.println("Salario: " + anderson.salario);
		anderson.receberAumento(500.0);
		anderson.mostrar();
		System.out.println("Salario anual: " + anderson.calcularGanhoAnual());
	}
}