class Funcionario{
	String nome;
	String departamento;
	double salario;
	Data dataEntrada = new Data();
	String rg;

	void receberAumento(double aumento){
		this.salario += aumento;
	}

	double calcularGanhoAnual(){
		return this.salario * 12;
	}

	void mostrar(){
		System.out.println("Nome: " + this.nome);
		System.out.println("Departamento: " + this.departamento);
		System.out.println("Salário: " + this.salario);
		System.out.println("Data de Entrada: " +this.dataEntrada.escreverData());
		System.out.println("RG: " + this.rg);
	}
}

class Data{
	int dia;
	int mes;
	int ano;

	void preencheData(int dia, int mes, int ano){
		this.dia = dia;
		this.mes = mes;
		this.ano = ano;
	}

	String escreverData(){
		return dia+"/"+mes+"/"+ano;
	}
}

class Empresa{
	String nome;
	String cnpj;
	int livre = 0;
	Funcionario[] empregados;

	void adicionar(Funcionario f){
		this.empregados[livre] = f; 
		livre++;

	}

	void mostrarEmpregados(){
		for(int i = 0; i < this.livre; i++){
			System.out.println("Funcionario da Posição '"+ i + "'\nSalario: " + this.empregados[i].salario);
		}
	}

	void mostrarTodasInformacoes(){
		for(int i = 0; i< this.livre; i++){
			this.empregados[i].mostrar();
		}
	}

	boolean contem(Funcionario f){
		for(int i = 0 ; i<this.livre; i++){
			if(f == this.empregados[i]){
				return true;
			}
		}
		return false;
	}
}

class Programa{
	public static void main(String[] args){
	
		Empresa empresa = new Empresa();
		empresa.empregados = new Funcionario[5];

		for(int i=0; i<5;i++){
			Funcionario f = new Funcionario ();
			f.salario= 1000 + i * 100;
			empresa.adicionar(f);
		}

		Funcionario anderson = new Funcionario();

		if(empresa.contem(empresa.empregados[2])){
			System.out.println("Dentro");
		}else{
			System.out.println("Sai");
		}

		if(empresa.contem(anderson)){
			System.out.println("Dentro");
		}else{
			System.out.println("Fora");
		}

	}
}