class Funcionario{
	private String nome;
	private String departamento;
	private double salario;
	public Data dataEntrada = new Data();
	private String rg;
	private static int indentificador = 0;

	public Funcionario(String nome){
		this.nome = nome;
		this.indentificador++;
	}

	public Funcionario(){
		this.indentificador++;
	}

	public int getIndentificador(){
		return this.indentificador;
	}

	public double getSalario(){
		return this.salario;
	}

	public void setSalario(double salario){
		this.salario = salario;
	}

	public void receberAumento(double aumento){
		this.salario += aumento;
	}

	public double getGanhoAnual(){
		return this.salario * 12;
	}

	public void mostrar(){
		System.out.println("Nome: " + this.nome);
		System.out.println("Departamento: " + this.departamento);
		System.out.println("Salário: " + getSalario());
		System.out.println("Data de Entrada: " +this.dataEntrada.escreverData());
		System.out.println("RG: " + this.rg);
	}
}
