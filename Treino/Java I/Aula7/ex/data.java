
class Data{
	int dia;
	int mes;
	int ano;


	public void preencheData(int dia, int mes, int ano){
		this.dia = dia;
		this.mes = mes;
		this.ano = ano;

		if(!validaData()){
			System.out.println("Data Invalida");
		}
	}

	private boolean validaData(){
		if(this.mes >0 && this.mes <13){
			if(this.dia >0 && this.dia <32){
				if( (this.mes == 1) || (this.mes == 3) || (this.mes == 5) || (this.mes == 7) || (this.mes == 8) || (this.mes == 10) || (this.mes == 12)){
					if(this.dia<32){
						return true;
					}
				}else{
					if(this.mes == 2){
						if ((ano % 400 == 0) || ((ano % 4 == 0) && (ano % 100 != 0))){
							if(this.dia <30){
								return true;
							}
						}else{
							if(this.dia <29){
								return true;
							}
						}
					}else{
						if(this.dia <31){
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public String escreverData(){
		return dia+"/"+mes+"/"+ano;
	}
}
