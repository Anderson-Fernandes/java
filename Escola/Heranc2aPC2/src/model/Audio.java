 /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author ESTAGIÁRIO
 */
public class Audio extends Eletronico {
 
    private int volume, potencial;

    public Audio(int potencial) {
        this.potencial = potencial;
    }
   
    public int getVolume() {
        return volume;
    }
    
    public void setVolume(int volume) {
        this.volume = volume;
    }
    
    public int getPotencial() {
        return potencial;
    }
    
    public void setPotencial(int potencial) {
        this.potencial = potencial;
    }
    
    public String reproduzir(){
        super.isLigado();
        return super.isLigado() ? "Reproduzindo Audio" : "Ligue o dispositivo primeiro";
    }
    
    public String pausar(){
        return "Pause";
    }
    
    public String avancaVoltar(int valor){
        return "Tempo de reprodução: " + valor;
    }
}
