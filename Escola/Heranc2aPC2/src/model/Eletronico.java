/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author ESTAGIÁRIO
 */
public class Eletronico {

    private int voltagem;
    private String modelo;
    private String marca;
    private boolean ligado;
    
    public void ligar(){
        ligado=true;
    }
    
    public void desligar(){
        ligado=false;
    }

    public int getVoltagem() {
        return voltagem;
    }

    public String getModelo() {
        return modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setVoltagem(int voltagem) {
        this.voltagem = voltagem;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setLigado(boolean ligado) {
        this.ligado = ligado;
    }
    
    

    public boolean isLigado() {
        return ligado;
    }
    
    
}
