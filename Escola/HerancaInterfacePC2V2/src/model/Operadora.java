/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author anali
 */
public interface Operadora {
    
    public boolean defineOperadora(String operadora);
    
    public String exibePlano();
    
    public boolean cadastraPlano();
    
}
