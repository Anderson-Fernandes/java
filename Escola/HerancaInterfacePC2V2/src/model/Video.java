/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author anali
 */
public class Video extends Eletronico{
    private int tela;
    private int contraste;
    private int brilho;

    public Video(int tela) {
        this.tela = tela;
    }

    public int getTela() {
        return tela;
    }

    public void setTela(int tela) {
        this.tela = tela;
    }

    public int getContraste() {
        return contraste;
    }

    public void setContraste(int contraste) {
        this.contraste = contraste;
    }

    public int getBrilho() {
        return brilho;
    }

    public void setBrilho(int brilho) {
        this.brilho = brilho;
    }
    
    public void reproduzir(){
        
    }
    public void pausa(){
        
    }
    public void avancaVolta(int valor){
        
    }
}
