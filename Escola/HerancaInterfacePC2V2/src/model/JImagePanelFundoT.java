/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package model;


import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * Especialização do painel do Vinícius.
 * Configura o arquivo de imagem explicitamente.
 *
 * @author David Buzatto
 */
public class JImagePanelFundoT extends JImagePanel {

    public JImagePanelFundoT() {

        try {
            setImage( ImageIO.read( getClass().getResourceAsStream( "/img/nokia-x3.png" ) ) );
        } catch ( IOException exc ) {
        }

    }

}
