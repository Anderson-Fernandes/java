/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package model;


import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * Especialização do painel do Vinícius.
 * Configura o arquivo de imagem explicitamente.
 *
 * @author David Buzatto
 */
public class JImagePanelFundoA extends JImagePanel {

    public JImagePanelFundoA() {

        try {
            setImage( ImageIO.read( getClass().getResourceAsStream( "/img/audio-front.png" ) ) );
        } catch ( IOException exc ) {
        }

    }

}
