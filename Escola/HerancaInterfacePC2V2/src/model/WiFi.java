/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author anali
 */
public interface WiFi {
    
    public boolean addRede(String nome, String senha);
    
    public boolean conecta();
    
    public boolean desconecta();
    
}
