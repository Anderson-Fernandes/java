/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author anali
 */
public class Audio extends Eletronico{
    private int volume, potencia;

    public Audio(int potencia) {
        this.potencia = potencia;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public int getPotencia() {
        return potencia;
    }

    public void setPotencia(int potencia) {
        this.potencia = potencia;
    }
    
    
    public String reproduzir(){
        if(super.isLigado()){
        return "Reproduzindo audio";
        }
        else {
            return "Ligue o dispositivo primeiro";
        }
    }
    public String pausa(){
        return "Pause";
    }
    public String avancaVolta(int valor){
        return "Tempo de reprodução: "+valor;
    }
}
