/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package view;

import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * Especialização do painel do Vinícius.
 * Configura o arquivo de imagem explicitamente.
 *
 * @author David Buzatto
 */
public class JImagePanelFundo extends JImagePanel {

    public JImagePanelFundo() {

        try {
            setImage( ImageIO.read( getClass().getResourceAsStream( "/painelimagem/imagens/duke2.png" ) ) );
        } catch ( IOException exc ) {
        }

    }

}
