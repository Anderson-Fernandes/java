/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio;

import javax.swing.JOptionPane;

/**
 *
 * @author Aluno
 */
public class Loja {


    
    public String nome, cidade, gerente;
    public Double faturamento;
    
    
    public Loja() {
    }
     public Loja(String nome, String cidade, String gerente, Double faturamento){
        this.nome=nome;
        this.cidade=cidade;
        this.gerente=gerente;
        this.faturamento=faturamento;
    }
     public Loja(String nome, String cidade, String gerente){
        this.nome=nome;
        this.cidade=cidade;
        this.gerente=gerente;
     }        

    //gets and sets
    public String getNome() {
        return nome;
    }

    public String getCidade() {
        return cidade;
    }

    public String getGerente() {
        return gerente;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
        }
    
    public void setGerente(String gerente) {
        this.gerente = gerente;
        }
  
    public void setFaturamento(Double faturamento) {
        
        if(faturamento < 0){
            this.faturamento = faturamento;
        }
        else{
            JOptionPane.showMessageDialog(null,"Valores do faturamento invalidos");
        }
    }
}
