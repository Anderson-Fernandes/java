/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import javax.swing.JFileChooser;

/**
 *
 * @author PC-CASA
 */
public class Compactador {
    private static final String BARRA = "/"; //
	private static final String BARRA_INVERTIDA = "\\"; 
	private static final int TAMANHO_BUFFER = 512000; //Utilizar buffer para acelerar o processo
        
        
        public void compactando(String opcao, String origem, String destino){
            try {
			
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));  //Leitura do Buffer
                        
                        // Escolha de Opções
			System.out.println("opcao [c = compactar] [d = descompactar] [s = sair]: ");
			String operacao = opcao; // ler resposta
                        // Verificação da resposta //
			if ("c".equalsIgnoreCase(operacao)) {
				System.out.println("COMPACTAR");
				System.out.println("arquivo(s) a ser(em) compactado(s): "); //Nome do Arquivo
				
				System.out.println("arquivo de destino:                 "); //Pasta para salvar
		
                                
                                
				String[] origens = separaArquivos(origem);
                                                  System.out.println(origem);
                                                                System.out.println(destino);
				compacta(origens, destino);
			} else if ("d".equalsIgnoreCase(operacao)) {
				System.out.println("DESCOMPACTAR");
				System.out.println("arquivo a ser descompactado:        ");
				System.out.println("diretorio de destino:               ");
                                System.out.println(origem);
                                                                System.out.println(destino);

				descompacta(origem, destino);
			} 
			System.out.println("concluido");
		} catch (Throwable t) {
			t.printStackTrace();
		}
        }

        
        /*                 COMPACTADOR                     */
	public void compacta(String[] origens, String destino) throws IOException {
		int length = (origens == null) ? 0 : origens.length;  //Se não existir arquivo, devolver 0
		if (length == 0 || destino == null) {
			return; //Se o arquivo não existir retornar
		}
               
		FileOutputStream fos = new FileOutputStream(destino); //Caminho de exportação do arquivo
		ZipOutputStream zos = new ZipOutputStream(fos); //Exportar arquivo ZIP
		zos.setMethod(ZipOutputStream.DEFLATED); //methodo de compressão
		File file = new File(origens[0]).getParentFile(); 
		String root = (file == null) ? null : file.getAbsolutePath();
		for (int i = 0; i < length; i++) {
			file = new File(origens[i]);
			if (file.isDirectory()) {
				varreDiretorio(root, origens[i], zos);
			} else {
				adicionaArquivo(root, origens[i], zos);
			}
		}
		zos.close();
	}
        
        
	public void descompacta(String origem, String destino) throws IOException {
		if (origem == null || destino == null) {
			return;
		}
		FileInputStream fis = new FileInputStream(origem); // Importar o arquivo
		ZipInputStream zis = new ZipInputStream(fis); // Criar arquivo ZIP
		FileOutputStream fos = null;    // Salvar arquivo ZIP
		BufferedOutputStream bos = null; //Buffer de salvamento
		ZipEntry ze = null; //Entrada de arquivos ZIP
		String name = null; // Nome do ZIP
		while ((ze = zis.getNextEntry()) != null) {
                    /*      Montando diretorio */
			name = destino + BARRA_INVERTIDA + ze.getName(); 
			try {
				fos = new FileOutputStream(name);
			} catch (FileNotFoundException exc) {
				montaDiretorio(name);
				fos = new FileOutputStream(name);
			}
			bos = new BufferedOutputStream(fos, TAMANHO_BUFFER);
			System.out.println("descompactando: '" + ze.getName() + "'");
			int bytes;
			byte buffer[] = new byte[TAMANHO_BUFFER];
			while ((bytes = zis.read(buffer, 0, TAMANHO_BUFFER)) != -1) {
				bos.write(buffer, 0, bytes);
			}
			bos.flush();
			bos.close();
		}
		zis.close();
	}
	private void adicionaArquivo(final String raiz, final String arquivo,
			final ZipOutputStream saida) throws IOException {
            
		if (arquivo.length() > 0) { //Verificar se o arquivo é maior que 0
			int length = (raiz == null) ? 0 : raiz.length();
			String name = (length > 0) ? arquivo.substring(length) : arquivo;
			name = name.replaceAll(BARRA, BARRA_INVERTIDA); //Trocar nome do direitorio de \ para \\
			while (name.startsWith(BARRA_INVERTIDA)) {
				name = name.substring(1);
			}
			System.out.println("compactando '" + name + "'"); //Informando que está compactando
			ZipEntry entry = new ZipEntry(name); //Nome do arquivo
			saida.putNextEntry(entry); //Entrada de arquivo
			FileInputStream fis = new FileInputStream(arquivo);  //
			int offset = 0;
			byte[] buffer = new byte[TAMANHO_BUFFER];
			while ((offset = fis.read(buffer, 0, TAMANHO_BUFFER)) != -1) {
				saida.write(buffer, 0, offset);
			}
			fis.close();
		}
	}
	private String[] listaConteudo(File diretorio) throws IOException {
		File[] files = diretorio.listFiles();
		int length = files == null ? 0 : files.length;
		String[] children = new String[length];
		File f = null;
		for (int i = 0; i < length; i++) {
			children[i] = files[i].getCanonicalPath();
		}
		return children;
	}
	private void montaDiretorio(String nome) throws IOException {
		File f;
		StringBuffer sb = new StringBuffer();
		StringTokenizer st = new StringTokenizer(nome, BARRA_INVERTIDA);
		int tokens = st.countTokens() - 1;
		for (int i = 0; i < tokens; i++) {
			sb.append(st.nextToken() + BARRA_INVERTIDA);
		}
		f = new File(sb.toString());
		f.mkdirs();
	}
        
        /*                 Ler todos os arquivos           */
	private String[] separaArquivos(String texto) {
		StringTokenizer st = new StringTokenizer(texto, ";");
		int tokens = st.countTokens();
		String[] array = new String[tokens];
		for (int i = 0; i < tokens; i++) {
			array[i] = st.nextToken();
		}
		return array;
	}
        
        
	private void varreDiretorio(final String raiz, final String diretorio,
			final ZipOutputStream saida) throws IOException {
            
		File file = new File(diretorio); //Selecionar arquivo
		String[] files = file.list(); //Listar os arquivos
		int length = (files == null) ? 0 : files.length; //tamanho do arquivo
		String caminho = null; //Caminho do arquivo
		for (int i = 0; i < length; i++) { //Laço continuo até o i for maior que o tamanho do arquivo
			file = new File(diretorio, files[i]); //Salvamento do arquivo
			caminho = file.getCanonicalPath(); //Caminho do arquivo
			if (file.isDirectory()) { //
				varreDiretorio(raiz, caminho, saida);
			} else {
				adicionaArquivo(raiz, caminho, saida);
			}
		}
		saida.flush();
	}
        
        public File[] escolhaDiretorio(){
        File[] arquivos  = null;
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Escolha o(s) arquivo(s)...");
        fc.setDialogType(JFileChooser.OPEN_DIALOG);
        fc.setApproveButtonText("OK");
        fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        fc.setMultiSelectionEnabled(true);
        int resultado = fc.showOpenDialog(fc);
        if (resultado == JFileChooser.APPROVE_OPTION){
             arquivos = fc.getSelectedFiles();
            
        }
            return arquivos;
        }
}
