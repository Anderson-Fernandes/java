/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio13;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Aluno
 */
public class calculadora extends JFrame {
    JTextField txt_n1;
    JTextField txt_n2;
    JButton btn_soma;
    JButton btn_subtracao;
    JButton btn_divisao;
    JButton btn_multiplicacao;
    JButton btn_calcular;
    JTextField txt_resultado;
    int op;
    double resultado;
    
    public calculadora() {
        setSize(245, 235);
        setLocationRelativeTo(null);
        setResizable(false);
        setLayout(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        txt_n1 = new JTextField();
        txt_n1.setBounds(20,20,195,20);
        txt_n2 = new JTextField();
        txt_n2.setBounds(20,50,195,20);
        btn_soma = new JButton("+");
        btn_soma.setBounds(20,80,41,41);
        btn_subtracao = new JButton("-");
        btn_subtracao.setBounds(70,80,41,41);
        btn_divisao = new JButton("/");
        btn_divisao.setBounds(120,80,41,41);
        btn_multiplicacao = new JButton("*");
        btn_multiplicacao.setBounds(170,80,41,41);
        btn_calcular = new JButton("Calcular");
        btn_calcular.setBounds(20,130,195,25);
        txt_resultado = new JTextField();
        txt_resultado.setBounds(20,165,195,20);
        add(txt_n1);
        add(txt_n2);
        add(btn_soma);
        add(btn_subtracao);
        add(btn_divisao);
        add(btn_multiplicacao);
        add(txt_resultado);
        add(btn_calcular);
        
       btn_soma.addActionListener(new ActionListener()
       {

            @Override
            public void actionPerformed(ActionEvent e) 
            {
                op=1;
            }
            
        });
       
        btn_subtracao.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e) 
            {
                op=2;
            }
            
        });
        
         btn_divisao.addActionListener(new ActionListener() 
         {

            @Override
            public void actionPerformed(ActionEvent e) 
            {
                op=3;
            }
            
        });
         
          btn_multiplicacao.addActionListener(new ActionListener() 
          {

            @Override
            public void actionPerformed(ActionEvent e) 
            {
                op=4;
            }
            
        });
          
          btn_calcular.addActionListener(new ActionListener() 
          {

            @Override
            public void actionPerformed(ActionEvent e) 
            {
                if (txt_n1.getText().isEmpty() || txt_n2.getText().isEmpty())
                {
                    JOptionPane.showMessageDialog(null, "Por Favor, preencha todos os campos!", "Dados Inválidos", JOptionPane.ERROR_MESSAGE);
                }
                else
                {
                    try
                    {
                        double n1 = Double.parseDouble(txt_n1.getText());
                        double n2 = Double.parseDouble(txt_n2.getText());
                        switch(op)
                        {
                            case 1: resultado = n1 + n2;
                                    break;
                            case 2: resultado = n1 - n2;
                                    break;
                            case 3 :resultado = n1 / n2;
                                    break;
                            case 4 :resultado = n1 * n2;
                                    break;
                            default:JOptionPane.showMessageDialog(null, "Por Favor, selecione uma operação", "Operação Não Selecionada", JOptionPane.ERROR_MESSAGE);
                        }
                        txt_resultado.setText(""+resultado);
                    }
                    catch(NumberFormatException ex)
                    {
                        JOptionPane.showMessageDialog(null, "Por Favor, digite apenas números!", "Dados Inválidos", JOptionPane.ERROR_MESSAGE);
                        txt_n1.setText("");
                        txt_n2.setText("");
                        txt_n1.requestFocus();
                    }
                }
            }
            
        });
}
}
