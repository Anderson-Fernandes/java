/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio12;

import javax.swing.JOptionPane;

/**
 *
 * @author Diany
 */
public class ExercicioRM03892 extends javax.swing.JFrame {

    /**
     * Creates new form ExercicioRM03892
     */
    public ExercicioRM03892() {
        initComponents();
        double media = 0, nota = 0;
        int qtdMedia = 0, cont = 0, n = 0;
        String relacao = "";
        setLocationRelativeTo(null);
        setResizable(false);
        try
        {
            n = Integer.parseInt(JOptionPane.showInputDialog(null, "Por favor, digite a quantidade de alunos que deseja cadastrar as notas", null, JOptionPane.QUESTION_MESSAGE));
        }
        catch (NumberFormatException ex)
        {
            JOptionPane.showMessageDialog(null, "Por favor, digite apenas valores numéricos", "Dados Inválidos", JOptionPane.INFORMATION_MESSAGE);
        }
        while(cont < n)
        {
            for(int contNota=0; contNota <4; contNota++)
            {
                nota = Double.parseDouble(JOptionPane.showInputDialog(null, (cont+1)+ "° Aluno" +"\nDigite a " + (contNota+1) + "° Nota", null));
                media = media + nota;
            }
            media = media / 4;
            relacao = relacao+"Aluno: "+(cont+1)+". Média: "+media+"\n";
            if (media >=5)
            {
                qtdMedia++;
            }
            cont++;
            media = 0;
        }
        if (qtdMedia > 0)
        {
            txt_resultado.setText("Foram Registrados "+n+" alunos.\n\nA quantidade de Alunos com a média aritmética maior que 5 é: "+qtdMedia);
        }
        else
        {
             txt_resultado.setText("Foram Registrados "+n+" alunos.\n\nNão há nenhum aluno com a média aritmética maior que 5.");
        }
        txt_resultado2.setText(relacao);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        txt_resultado = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        txt_resultado2 = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        txt_resultado.setEditable(false);
        txt_resultado.setColumns(2);
        txt_resultado.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        txt_resultado.setRows(5);
        jScrollPane1.setViewportView(txt_resultado);

        txt_resultado2.setEditable(false);
        txt_resultado2.setColumns(20);
        txt_resultado2.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        txt_resultado2.setRows(5);
        jScrollPane2.setViewportView(txt_resultado2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 417, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ExercicioRM03892.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ExercicioRM03892.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ExercicioRM03892.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ExercicioRM03892.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ExercicioRM03892().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea txt_resultado;
    private javax.swing.JTextArea txt_resultado2;
    // End of variables declaration//GEN-END:variables
}
