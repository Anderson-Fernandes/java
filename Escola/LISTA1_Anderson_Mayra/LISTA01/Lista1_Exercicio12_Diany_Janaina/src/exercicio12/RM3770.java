/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio12;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
/**
 *
 * @author KJ
 */
public class RM3770 {
    static int vtotal3770, x, y, dinheiro, ttroco;
     public static void main(String[] args)
     {
        JFrame tela = new JFrame("Aplicação Desktop");
        tela.setSize(400, 400);
        tela.setVisible(true);
        tela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        tela.setLocationRelativeTo(null);
        tela.setLayout(null);
        
        JButton total = new JButton("Calcular Total");
        total.setBounds(100, 50, 150, 50);
        tela.add(total);
        total.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
               
            x= Integer.parseInt(JOptionPane.showInputDialog("Digite a quantidade produtos: "));
            y = Integer.parseInt(JOptionPane.showInputDialog("Digite o valor unitário do produto: "));
            vtotal3770 = x * y;
               JOptionPane.showMessageDialog(null, "O valor total da compra é: R$ " +vtotal3770); 
                //System.exit(0);
            }
        });
        
        JButton troco = new JButton("Calcular Troco");
        troco.setBounds(100, 100, 150, 50);
        tela.add(troco);
        troco.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dinheiro = Integer.parseInt(JOptionPane.showInputDialog("Digite o valor pago pelo cliente: "));
                ttroco = dinheiro - vtotal3770;
                JOptionPane.showMessageDialog(null, "O troco é: R$ " +ttroco);
            }
            
        });
        
        JButton produtos = new JButton("Lista de Produtos");
        produtos.setBounds(100, 150, 150, 50);
        tela.add(produtos);
        produtos.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Mouse R$9,80\n"
                +"Teclado R$29,90\n"
                +"Cartão SD 8GB R$30,00\n"
                +"PenDrive 16GB R$ 46,00\n");
            }
            
        });
        
        JButton ajuda = new JButton("Ajuda");
        ajuda.setBounds(100, 200, 150, 50);
        tela.add(ajuda);
        ajuda.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Calcular Total > Calcula o valor total da compra\n"
                + "Calcular Troco > Calcula quanto de troco o cliente terá\n"
                + "Lista de Produtos > Exibe a lista de produtos\n"
                + "Sair > Encerra a compra");
            }
            
        });
        
        JButton sair = new JButton("Sair");
        sair.setBounds(100, 250, 150, 50);
        tela.add(sair);
        sair.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
    });
     }
}
