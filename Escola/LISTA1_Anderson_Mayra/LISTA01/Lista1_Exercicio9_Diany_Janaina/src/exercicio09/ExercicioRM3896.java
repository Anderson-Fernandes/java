/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio09;

import javax.swing.JOptionPane;

/**
 *
 * @author Aluno
 */
public class ExercicioRM3896 {
    public static void main (String []args) {
        int a=0, i2, media=0;
        String n3896 = JOptionPane.showInputDialog("Digite a quantidade de alunos: ");
        int cont = Integer.parseInt(n3896);
        for (int i = 0; i < cont; i++)
        {
            for (i2 = 0; i2 < 4; i2++)
            { 
                String nota = JOptionPane.showInputDialog("Digite a "+(i2+1)+" nota do "+(i+1)+"º aluno: ");
                int nota2 = Integer.parseInt(nota);
                media = nota2 + media;
            }
            media = media / 4;
            if (media>5)
            {
                a++;
            }
            media = 0;
        }
        if (a>0)
        {
            JOptionPane.showMessageDialog(null, +a+" aluno(s) estão com a nota acima de 5.0");
        }
        else
        {
            JOptionPane.showMessageDialog(null, "Não há nenhum aluno com a nota acima de 5.");
        }
    }
}
