-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.5.25


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema videoteca
--

CREATE DATABASE IF NOT EXISTS videoteca;
USE videoteca;

--
-- Definition of table `filme`
--

DROP TABLE IF EXISTS `filme`;
CREATE TABLE `filme` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `filme` varchar(45) NOT NULL,
  `cd_genero` int(11) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `fk_filme_genero` (`cd_genero`),
  CONSTRAINT `fk_filme_genero` FOREIGN KEY (`cd_genero`) REFERENCES `genero` (`cd_genero`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `filme`
--

/*!40000 ALTER TABLE `filme` DISABLE KEYS */;
INSERT INTO `filme` (`codigo`,`filme`,`cd_genero`) VALUES 
 (1,'Homem de Ferro',1),
 (4,'Nome',1),
 (5,'Avenger',1),
 (6,'SALT',1),
 (7,'ACTION',1),
 (8,'American Pie',2),
 (9,'Google > Microsoft',2),
 (10,'C# > JAVA',2),
 (11,'Final da Season 5',2),
 (12,'Harry Potter',3),
 (13,'Jogos Vorazes',3),
 (14,'Av',1);
/*!40000 ALTER TABLE `filme` ENABLE KEYS */;


--
-- Definition of table `genero`
--

DROP TABLE IF EXISTS `genero`;
CREATE TABLE `genero` (
  `cd_genero` int(11) NOT NULL AUTO_INCREMENT,
  `genero` varchar(45) NOT NULL,
  PRIMARY KEY (`cd_genero`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `genero`
--

/*!40000 ALTER TABLE `genero` DISABLE KEYS */;
INSERT INTO `genero` (`cd_genero`,`genero`) VALUES 
 (1,'Ação'),
 (2,'Comédia'),
 (3,'Aventura'),
 (4,'Drama'),
 (5,'Adulto'),
 (6,'Terror'),
 (7,'Clássico'),
 (8,'Musical');
/*!40000 ALTER TABLE `genero` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
