/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bdvideoteca;

/**
 *
 * @author ESTAGIÁRIO
 */
class Genero {
    
    private int codigo;
    private String nome;
    
    public Genero(int codigo, String nome){
        this.codigo=codigo;
        this.nome = nome;
    }
    
    public int getCodigo(){
        return this.codigo;
    }
    
    public String getNome(){
        return this.nome;
    }
}
