Atividade

Cadastrar funcionários para uma empresa
Nome (20 caracteres)
Idade - JSpinner (18 a 99)
Curos superior completo - sim, não (Radio Button)
Conhecimentos de informática (Word, Excel, Auto Cad, Photoshop) (checkbox)
Horas trabalhadas por dia (1 a 8) - JSpinner
Valor da hora trabalhada (Valor monetário)
Haverá acréscimo de Bônus se tem curso superior - 20%
Cada conhecimento em informática aumenta em 2% a hora trabalhada
Botão calcular - Salário previsto em 30 dias + Bônus + informática - Escrever na caixa de texto de salário final
Ao clicar no botão Cadastrar deverá ser apresentado todos os dados preenchidos do novo funcionário e o seu salário mensal em uma JDialog.
O botão cadastrar deve ser ativado somente após calcular o valor mensal com os acréscimos
Após fechar o JDialog os campos devem ser limpos e o botão cadastrar desativado
