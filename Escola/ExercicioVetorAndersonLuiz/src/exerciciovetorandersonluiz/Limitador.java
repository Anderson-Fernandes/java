/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exerciciovetorandersonluiz;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author Aluno
 */
public class Limitador extends PlainDocument {

    int maximo;

    public Limitador(int max) {
        maximo = max;
    }

    public void insertString(int offs, String str,
            AttributeSet a) throws BadLocationException {
        if ((getLength() + str.length()) <= maximo) {
            super.insertString(offs, str, a);
        }
    }
}

