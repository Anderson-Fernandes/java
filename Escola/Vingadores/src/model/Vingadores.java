/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Aluno
 */
public class Vingadores {
 ArrayList<String> cadastro; 
    
    public Vingadores() 
    {
         cadastro = new ArrayList<>();
    }
    public void adicionar(String nomeReal, String nomeHeroico, String poder, String fraqueza, String planetaOrigem)
    {        
        String juncao = nomeReal  + "\n" + nomeHeroico + "\n" + poder +  "\n" + fraqueza + "\n" + planetaOrigem + "\n";
        cadastro.add(juncao);
    }
    public void exibir()
    {
        String exibir = "Os Heroes cadastrado são: \n";
        for( int i=0;i<cadastro.size();i++)
        {
            exibir += cadastro.get(i);
        }
        exibir += "\n----------------\n";
        JOptionPane.showMessageDialog(null, exibir);
    }
}
