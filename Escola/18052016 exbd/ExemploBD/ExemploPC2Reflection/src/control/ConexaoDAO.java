/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Aluno
 */
public class ConexaoDAO {

    private Connection con;
    private String erro;

    public Connection conectar(String user, String senha) throws SQLException {
        if (con != null) {
            return con;
        } else {
            con = DriverManager.getConnection("jdbc:mysql://localhost/agenda", user, senha);
        }
        return con;
    }

    public String mostraErro() {
        return erro;
    }

    //Aqui será usado reflection, um recurso avançado para ler atributos e executar métodos
    //em tempo de execução
    public void inserir(String tabela, Object obj) throws Exception {
        String sql = "INSERT INTO " + tabela.toUpperCase() + " (";
        //buscar os atributos da classe informada por obj
        Class<?> classe = obj.getClass();
        //Os atributos serão manipulados pela Classe Field
        for (Field f : classe.getDeclaredFields()) {
            f.setAccessible(true);
            sql += f.getName() + ",";
        }
        //remover a vírgula do final
        sql = sql.substring(0, sql.length() - 1) + ") VALUES (";
        //Buscar os valores dos atributos do Objeto obj
        for (Field f : classe.getDeclaredFields()) {
            f.setAccessible(true);
            if (f.getType().getSimpleName().equals("String")) {
                sql += "'" + f.get(obj) + "',";
            } else {
                sql += f.get(obj) + ",";
            }
        }
        //remover a vírgula do final
        sql = sql.substring(0, sql.length() - 1) + ")";
        System.out.println(sql);
        //executar a query
        queryBD(sql);
    }

    /**
     * Este método realiza as funções de inserir, alterar e excluir
     *
     * @param sql
     * @return
     */
    public boolean queryBD(String sql) {
        try {
            Statement st = con.createStatement();
            int result = st.executeUpdate(sql);
            st.close();
            if (result >= 1) {
                return true;
            }
        } catch (SQLException ex) {
            erro = "Erro: " + ex.getMessage();
            return false;
        }
        return false;
    }
    public ResultSet resultSet(String tabela) throws SQLException{
        Statement st = con.createStatement();
        return st.executeQuery("select * from "+tabela);
    }
    
     public void alterar(String tabela, Object obj1, Object obj2) throws Exception{
        String  sql = "UPDATE " + tabela.toUpperCase()+" SET ";
        //Buscar os atributos da Classe informada
        Class<?> classe = obj1.getClass();
        //Um for para exibir os campos e completar a String SQL
        for(Field f : classe.getDeclaredFields()){
            f.setAccessible(true);
            //SE o valor for uma String então 
            if(f.getType().getSimpleName().equals("String")){
                sql+= f.getName()+ "='"+f.get(obj1)+"',";
            }else{
                sql+=f.getName()+"="+f.get(obj1)+",";
            }
        }
        
        //Remove a última virgula
        sql = sql.substring(0, sql.length()-1);
        sql+=" WHERE ";
        //buscar os atributos de obj2
        for (Field f : classe.getDeclaredFields()){
            f.setAccessible(true);
            
            if(f.getType().getSimpleName().equals("String")){
                sql+=f.getName()+"='"+f.get(obj2)+"' AND ";
            }else{
                sql+=f.getName()+"="+f.get(obj2)+" AND ";
            }
        }
        
        sql=sql.substring(0,sql.length()-5);
        System.out.println(sql);
        
        //executa a query
        
        queryBD(sql);
        
        
    }
     
     public void excluir(String tabela, Object obj1) throws Exception{
        String sql = "DELETE FROM " + tabela.toUpperCase() + " WHERE ";
        Class <?> classe = obj1.getClass();
        
        for(Field f: classe.getDeclaredFields()){
            f.setAccessible(true);
            
            if(f.getType().getSimpleName().equals("String")){
                sql+=f.getName()+"='"+f.get(obj1)+"' AND ";
            }else{
                sql+=f.getName()+"="+f.get(obj1)+" AND ";
            }
        }
        
        sql = sql.substring(0, sql.length()-5);
        System.out.println(sql);
         
        queryBD(sql);
        
        
     }
}
